
const firstNum = 2
const secondNum = 3
const cubicNum = Math.pow(firstNum, secondNum)

console.log(`The cube of ${firstNum} is ${cubicNum}`)


const address = [258, 'Washington Ave NW', 'California', 90011]
const [houseNum, street, city, stateNum] = address

console.log(`I live at ${houseNum} ${street}, ${city} ${stateNum}`)

const lolongWeight = 1075
const lengthFt = 20
const lengthIn = 3

console.log(`Lolong was a saltwater crocodile. He weight at ${lolongWeight} kgs with a measurement of ${lengthFt} ft and ${lengthIn} in.`)

const numbers = [1,2,3,4,5,15];

numbers.forEach((number) => {
    
        console.log(number)
})

class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed
    }
}

const dogDetails = new Dog('Frankie',5,'Miniature Dachshund');
console.log(dogDetails)